﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task9_AnimalPark
{
    class Animal
    {

        public Animal() { }
        public Animal(string name, int animalID, string sound, string species)
        {
            Name = name;
            Sound = sound;
            AnimalID = animalID;
            Species = species;
        }

        private string name;
        private string sound;
        private int animalID;
        private string species;

        public string Name { get => name; set => name = value; }
        public string Sound { get => sound; set => sound = value; }
        public int AnimalID { get => animalID; set => animalID = value; }
        public string Species { get => species; set => species = value; }

        public void MakeSound()
        {
            Console.WriteLine($"{Name}'s cry makes a {Sound} sound");
        }

        public override string ToString()
        {
            return $"{Name} with the ID {AnimalID} is an {Species} and makes a {Sound} sound";
        }
    }
}
