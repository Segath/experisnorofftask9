﻿using System;
using System.Collections.Generic;

namespace Task9_AnimalPark
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal billy = new Animal("Billy", 521, "Bah", "Goat");
            Animal stitch = new Animal("Stitch", 626, "Ohana", "Dog");
            Animal garfield = new Animal("Garfield", 001, "John", "Cat");

            List<Animal> listOfAnimals = new List<Animal>
            {
                billy,
                stitch,
                garfield
            };

            foreach(Animal a in listOfAnimals)
            {
                Console.WriteLine(a.ToString());
            }
        }
    }
}
